SHELL = /bin/sh
export ROOTS_VERSION=master

export COMPANY_NAME=SkaleSys
export COMPANY_WEBSITE=https://skalesys.com

export LOGO_THEME=technology,network

export PROJECT_NAME=terraform-vpc
export REGION=ap-southeast-2

export BUCKET=sk-prd-terraform-state
export DYNAMODB_TABLE=sk-prd-terraform-state-lock

-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)

.PHONY: install
## Install project requirements
install:
	@make --silent terraform/install
	@make --silent gomplate/install
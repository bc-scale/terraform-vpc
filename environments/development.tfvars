stage = "dev"

environment = "development"

#-----
# VPC
#-----

cidr = "10.1.0.0/16"

private_subnets = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"]

public_subnets = ["10.1.11.0/24", "10.1.12.0/24", "10.1.13.0/24"]

database_subnets = ["10.1.21.0/24", "10.1.22.0/24"]

elasticache_subnets = ["10.1.31.0/24", "10.1.32.0/24"]

intra_subnets = ["10.1.41.0/24", "10.1.42.0/24"]

## DNS
enable_dns_hostnames = "true"

enable_dns_support = "true"

assign_generated_ipv6_cidr_block = "true"

## One NAT Gateway per subnet
enable_nat_gateway = "true"

single_nat_gateway = "false"

one_nat_gateway_per_az = "false"

enable_vpn_gateway = "true"

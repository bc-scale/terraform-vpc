output "region" {
  value = "${var.region}"
}

output "account_id" {
  value = "${data.aws_caller_identity.current.account_id}"
}

output "user_id" {
  value = "${data.aws_caller_identity.current.user_id}"
}

output "arn" {
  value = "${data.aws_caller_identity.current.arn}"
}

output "vpc_label" {
  value = {
    id         = "${module.vpc_label.id}"
    name       = "${module.vpc_label.name}"
    namespace  = "${module.vpc_label.namespace}"
    stage      = "${module.vpc_label.stage}"
    attributes = "${module.vpc_label.attributes}"
  }
}

output "vpc_label_tags" {
  value = "${module.vpc_label.tags}"
}

output "vpc_label_context" {
  value = "${module.vpc_label.context}"
}

output "azs" {
  description = "A list of availability zones specified as argument to this module"

  value = "${module.vpc.azs}"
}

output "database_network_acl_id" {
  description = "ID of the database network ACL"

  value = "${module.vpc.database_network_acl_id}"
}

output "database_route_table_ids" {
  description = "List of IDs of database route tables"

  value = "${module.vpc.database_route_table_ids}"
}

output "database_subnet_group" {
  description = "ID of database subnet group"

  value = "${module.vpc.database_subnet_group}"
}

output "database_subnets" {
  description = "List of IDs of database subnets"

  value = "${module.vpc.database_subnets}"
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"

  value = "${module.vpc.database_subnets_cidr_blocks}"
}

output "elasticache_network_acl_id" {
  description = "ID of the elasticache network ACL"

  value = "${module.vpc.elasticache_network_acl_id}"
}

output "elasticache_route_table_ids" {
  description = "List of IDs of elasticache route tables"

  value = "${module.vpc.elasticache_route_table_ids}"
}

output "elasticache_subnet_group" {
  description = "ID of elasticache subnet group"

  value = "${module.vpc.elasticache_subnet_group}"
}

output "elasticache_subnet_group_name" {
  description = "Name of elasticache subnet group"

  value = "${module.vpc.elasticache_subnet_group_name}"
}

output "elasticache_subnets" {
  description = "List of IDs of elasticache subnets"

  value = "${module.vpc.elasticache_subnets}"
}

output "elasticache_subnets_cidr_blocks" {
  description = "List of cidr_blocks of elasticache subnets"

  value = "${module.vpc.elasticache_subnets_cidr_blocks}"
}

output "igw_id" {
  description = "The ID of the Internet Gateway"

  value = "${module.vpc.igw_id}"
}

output "intra_network_acl_id" {
  description = "ID of the intra network ACL"

  value = "${module.vpc.intra_network_acl_id}"
}

output "intra_route_table_ids" {
  description = "List of IDs of intra route tables"

  value = "${module.vpc.intra_route_table_ids}"
}

output "intra_subnets" {
  description = "List of IDs of intra subnets"

  value = "${module.vpc.intra_subnets}"
}

output "intra_subnets_cidr_blocks" {
  description = "List of cidr_blocks of intra subnets"

  value = "${module.vpc.intra_subnets_cidr_blocks}"
}

output "nat_ids" {
  description = "List of allocation ID of Elastic IPs created for AWS NAT Gateway"

  value = "${module.vpc.nat_ids}"
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"

  value = "${module.vpc.nat_public_ips}"
}

output "natgw_ids" {
  description = "List of NAT Gateway IDs"

  value = "${module.vpc.natgw_ids}"
}

output "private_network_acl_id" {
  description = "ID of the private network ACL"

  value = "${module.vpc.private_network_acl_id}"
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"

  value = "${module.vpc.private_route_table_ids}"
}

output "private_subnets" {
  description = "List of IDs of private subnets"

  value = "${module.vpc.private_subnets}"
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"

  value = "${module.vpc.private_subnets_cidr_blocks}"
}

output "public_network_acl_id" {
  description = "ID of the public network ACL"

  value = "${module.vpc.public_network_acl_id}"
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"

  value = "${module.vpc.public_route_table_ids}"
}

output "public_subnets" {
  description = "List of IDs of public subnets"

  value = "${module.vpc.public_subnets}"
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"

  value = "${module.vpc.public_subnets_cidr_blocks}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"

  value = "${module.vpc.vpc_cidr_block}"
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"

  value = "${module.vpc.vpc_enable_dns_hostnames}"
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support"

  value = "${module.vpc.vpc_enable_dns_support}"
}

output "vpc_id" {
  description = "The ID of the VPC"

  value = "${module.vpc.vpc_id}"
}

output "vpc_instance_tenancy" {
  description = "Tenancy of instances spin up within VPC"

  value = "${module.vpc.vpc_instance_tenancy}"
}

output "vpc_main_route_table_id" {
  description = "The ID of the main route table associated with this VPC"

  value = "${module.vpc.vpc_main_route_table_id}"
}

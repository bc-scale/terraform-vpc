provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.13"
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "skalesys"

    workspaces {
      prefix = "vpc-"
    }
  }
}

data "aws_caller_identity" "current" {}

#-------
# LABELS
#-------

locals {
  tags = "${merge(var.tags, map("Environment", "${var.environment}"))}"
}

module "vpc_label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git"
  version    = "0.6.3"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["vpc"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.60.0"

  name = "${module.vpc_label.id}"

  cidr = "${var.cidr}"

  azs                 = "${var.availability_zones}"
  private_subnets     = "${var.private_subnets}"
  public_subnets      = "${var.public_subnets}"
  intra_subnets       = "${var.intra_subnets}"
  database_subnets    = "${var.database_subnets}"
  elasticache_subnets = "${var.elasticache_subnets}"

  assign_generated_ipv6_cidr_block = "${var.assign_generated_ipv6_cidr_block}"

  enable_nat_gateway     = "${var.enable_nat_gateway}"
  single_nat_gateway     = "${var.single_nat_gateway}"
  one_nat_gateway_per_az = "${var.one_nat_gateway_per_az}"

  enable_dns_support   = "${var.enable_dns_support}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"
  enable_vpn_gateway   = "${var.enable_vpn_gateway}"

  vpc_tags = "${module.vpc_label.tags}"
}

